package nl.ddeboer.bootcalc.service.validation;

import nl.ddeboer.bootcalc.service.validation.exception.ValueValidationException;
import org.junit.Test;
import nl.ddeboer.bootcalc.service.strategy.CalculationStrategy.Operation;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class ValueValidatorTest {


    @Test(expected = ValueValidationException.class)
    public void testIntegerListMayNotBeEmpty() {
        List<BigDecimal> bigDecimals = List.of();
        ValueValidator.ValueType.integer.validate(bigDecimals, Operation.add);
    }

    @Test(expected = ValueValidationException.class)
    public void testIntegerListMayNotBeNull() {
        List<BigDecimal> bigDecimals = null;
        ValueValidator.ValueType.integer.validate(bigDecimals, Operation.add);
    }

    @Test
    public void testIntegerListPasses() {
        List<BigDecimal> bigDecimals = List.of(BigDecimal.ONE, BigDecimal.TEN);
        ValueValidator.ValueType.integer.validate(bigDecimals, Operation.add);
    }

    @Test(expected = ValueValidationException.class)
    public void testIntegerListMayNotContainDecimals() {
        List<BigDecimal> bigDecimals = List.of(BigDecimal.ONE, BigDecimal.valueOf(0.5));
        ValueValidator.ValueType.integer.validate(bigDecimals, Operation.add);
    }

    @Test(expected = ValueValidationException.class)
    public void testIntegerListMayNotContainNull() {
        List<BigDecimal> bigDecimals = Arrays.asList(BigDecimal.ONE, null);
        ValueValidator.ValueType.integer.validate(bigDecimals, Operation.add);
    }

    // decimals
    @Test(expected = ValueValidationException.class)
    public void testDecimalListMayNotContainNull() {
        List<BigDecimal> bigDecimals = Arrays.asList(BigDecimal.ONE, null);
        ValueValidator.ValueType.decimal.validate(bigDecimals, Operation.add);
    }

    @Test
    public void testDecimalListNoException() {
        List<BigDecimal> bigDecimals = Arrays.asList(BigDecimal.valueOf(0.6), BigDecimal.valueOf(34543.0));
        ValueValidator.ValueType.decimal.validate(bigDecimals, Operation.add);
    }

    @Test(expected = ValueValidationException.class)
    public void testDecimalListEntryTooBig() {
        List<BigDecimal> bigDecimals = Arrays.asList(BigDecimal.valueOf(Double.MAX_VALUE), BigDecimal.valueOf(34543.0));
        ValueValidator.ValueType.decimal.validate(bigDecimals, Operation.add);
    }

    // save
    @Test(expected = ValueValidationException.class)
    public void testSaveListMayNotContainNull() {
        List<BigDecimal> bigDecimals = Arrays.asList(BigDecimal.ONE, null);
        ValueValidator.ValueType.safe.validate(bigDecimals, Operation.add);
    }

    @Test
    public void testSaveListNoException() {
        List<BigDecimal> bigDecimals = Arrays.asList(BigDecimal.valueOf(0.6), BigDecimal.valueOf(34543.0));
        ValueValidator.ValueType.safe.validate(bigDecimals, Operation.add);
    }

}
