package nl.ddeboer.bootcalc.controller;

import nl.ddeboer.bootcalc.service.CalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class CalculatorController {

    private CalculationService calculationService;

    @Autowired
    public CalculatorController(CalculationService calculationService) {
        this.calculationService =calculationService;
    }

    @PostMapping("/calculate")
    public ResponseEntity calculate(@Valid @RequestBody CalculationRequest request) {
        CalculationResult result = calculationService.calculate(request);
        return new ResponseEntity(result, HttpStatus.OK);
    }


}

