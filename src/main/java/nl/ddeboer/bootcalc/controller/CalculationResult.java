package nl.ddeboer.bootcalc.controller;

import lombok.Data;

import java.math.BigDecimal;


/**
 * I return a bigdecimal for all calculations.
 */
@Data
public class CalculationResult {

    private BigDecimal result;



    public static CalculationResult of(BigDecimal result) {
        CalculationResult cr = new CalculationResult();
        cr.setResult(result);
        return cr;
    }
}
