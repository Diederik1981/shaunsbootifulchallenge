package nl.ddeboer.bootcalc.controller;


import lombok.Data;
import nl.ddeboer.bootcalc.service.validation.ValueValidator.ValueType;
import nl.ddeboer.bootcalc.service.strategy.CalculationStrategy.Operation;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

//POST /calculate
//        {
//        "values": [5, 1, 20],
//        "operation": "add",
//        "type": "integer"
//        }
@Data
public class CalculationRequest {


    @NotNull(message = "values must be supplied")
    List<BigDecimal> values;

    @NotNull(message = "operation must be supplied")
    Operation operation;

    @NotNull(message = "type must be supplied")
    ValueType type;

    public static void main(String[] args) {
        String arr[] = new String[50];
    }


}
