package nl.ddeboer.bootcalc.core;

/**
 * CoreException needs a message for this project.
 */
public class CoreException extends RuntimeException {

    public CoreException(String message){
        super(message);
    }

}
