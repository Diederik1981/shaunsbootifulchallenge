package nl.ddeboer.bootcalc.service.strategy;

import nl.ddeboer.bootcalc.core.CoreException;

import java.math.BigDecimal;
import java.util.List;

public class Sub implements CalculationStrategy {

    @Override
    public BigDecimal calculateInteger(List<BigDecimal> bigDecimals) {
        return bigDecimals.stream().reduce(BigDecimal::subtract).orElse(BigDecimal.ZERO);
    }

    @Override
    public BigDecimal calculateDecimal(List<BigDecimal> bigDecimals) {
        BigDecimal result = bigDecimals.stream().reduce(BigDecimal::subtract).orElse(BigDecimal.ZERO);
        if (result.intValue() != result.longValue()) {
            throw new CoreException("result too small for decimal.");
        }
        return result;
    }

    @Override
    public BigDecimal calculateSave(List<BigDecimal> bigDecimals) {
        return bigDecimals.stream().reduce(BigDecimal::subtract).orElse(BigDecimal.ZERO);
    }
}
