package nl.ddeboer.bootcalc.service.strategy;

import nl.ddeboer.bootcalc.core.CoreException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public class Div implements CalculationStrategy {


    @Override
    public BigDecimal calculateInteger(List<BigDecimal> bigDecimals) {
        BigInteger bi = bigDecimals.stream().map(BigDecimal::toBigInteger).reduce(BigInteger::divide).orElse(BigInteger.ZERO);
        return new BigDecimal(bi);
    }

    @Override
    public BigDecimal calculateDecimal(List<BigDecimal> bigDecimals) {
        BigDecimal result = bigDecimals.stream().reduce(BigDecimal::divide).orElse(BigDecimal.ZERO);
        if (result.intValue() != result.longValue()) {
            throw new CoreException("result too big or too small for decimal.");
        }
        return result;
    }

    @Override
    public BigDecimal calculateSave(List<BigDecimal> bigDecimals) {
        return bigDecimals.stream().reduce(BigDecimal::divide).orElse(BigDecimal.ZERO);
    }

}
