package nl.ddeboer.bootcalc.service.strategy;

import nl.ddeboer.bootcalc.core.CoreException;
import nl.ddeboer.bootcalc.service.validation.ValueValidator.ValueType;

import java.math.BigDecimal;
import java.util.List;

public interface CalculationStrategy {

    /**
     *  add, 	// Will add all the values together 	[5, 10, 2] => 17
     *  sub, 	// Will subtract all the values, starting at the left-most value, and ending at the right-most value. 	[10, 2, 2] => 6
     *  mul, 	// Will multiply all the values together. 	[5, 5, 2] => 50
     *  div; 	// Will divide all the values, starting at the left-most value, and ending at the right-most value. 	[10, 1, 2] => 5
     */
    enum Operation {

        add,
        sub,
        mul,
        div;



    }

    default BigDecimal calculate(List<BigDecimal> bigDecimals, ValueType vt) {
        switch (vt) {
            case integer:
                return calculateInteger(bigDecimals);
            case decimal:
                return calculateDecimal(bigDecimals);
            case safe:
                return calculateSave(bigDecimals);
            default:
                throw new CoreException("Operation not implemented");
        }
    }

    BigDecimal calculateInteger(List<BigDecimal> bigDecimals);
    BigDecimal calculateDecimal(List<BigDecimal> bigDecimals);
    BigDecimal calculateSave(List<BigDecimal> bigDecimals);




}
