package nl.ddeboer.bootcalc.service;


import nl.ddeboer.bootcalc.controller.CalculationRequest;
import nl.ddeboer.bootcalc.controller.CalculationResult;

public interface CalculationService {

    /**
     * Takes a calculation request and returns the result of the chosen calculation.
     */
    CalculationResult calculate(CalculationRequest request);
}
