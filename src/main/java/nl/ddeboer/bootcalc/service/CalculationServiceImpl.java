package nl.ddeboer.bootcalc.service;

import nl.ddeboer.bootcalc.controller.CalculationRequest;
import nl.ddeboer.bootcalc.controller.CalculationResult;
import nl.ddeboer.bootcalc.core.CoreException;
import nl.ddeboer.bootcalc.service.strategy.Add;
import nl.ddeboer.bootcalc.service.strategy.Div;
import nl.ddeboer.bootcalc.service.strategy.Mul;
import nl.ddeboer.bootcalc.service.strategy.Sub;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class CalculationServiceImpl implements CalculationService {


    @Override
    public CalculationResult calculate(CalculationRequest request) {
        request.getType().validate(request.getValues(), request.getOperation());

        BigDecimal result;
        switch (request.getOperation()) {
            case add:
                result = new Add().calculate(request.getValues(), request.getType());
                break;
            case mul:
                result = new Mul().calculate(request.getValues(), request.getType());
                break;
            case div:
                result = new Div().calculate(request.getValues(), request.getType());
                break;
            case sub:
                result = new Sub().calculate(request.getValues(), request.getType());
                break;
            default:
                throw new CoreException("Unexpected value: " + request.getOperation());
        }

        return CalculationResult.of(result);
    }








}