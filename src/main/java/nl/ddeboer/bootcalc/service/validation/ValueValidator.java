package nl.ddeboer.bootcalc.service.validation;

import nl.ddeboer.bootcalc.service.strategy.CalculationStrategy.Operation;
import nl.ddeboer.bootcalc.service.validation.exception.ValueValidationException;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public final class ValueValidator {

    private ValueValidator() {
        throw new RuntimeException("Nothing to see here !, cannot instantiate");
    }


    /**
     * integer : The values will be processed as whole numbers, any decimals passed into values should return an error.
     * decimal() : The values will be processed as decimal numbers, 32-bits of precision will suffice.
     * safe() : The values will be processed as safe decimals, you must ensure there are no floating-point precision errors when applying operations to safe decimals.
     */
    public enum ValueType {

        integer(integerListValidator), // The values will be processed as whole numbers, any decimals passed into values should return an error.
        decimal(decimalListValidator), // The values will be processed as decimal numbers, 32-bits of precision will suffice.
        safe(i -> {}); // The values will be processed as safe decimals, you must ensure there are no floating-point precision errors when applying operations to safe decimals.

        // validator specific to datatype:
        Consumer<List<BigDecimal>> validator;

        ValueType(Consumer<List<BigDecimal>> validator) {
            this.validator = validator;

        }


        public void validate(List<BigDecimal> list, Operation op) {

            // first do checks for all lists
            commonValidator.accept(list, op);

            // than use the validator specific to the enum
            validator.accept(list);

        }

    }

    /**
     * This validator should be used for all the values.
     */
    static final BiConsumer<List<BigDecimal>, Operation> commonValidator = (list, op) ->  {
        if (list == null || list.isEmpty()) {
            throw new ValueValidationException("list may not be empty");
        }
        for (BigDecimal bd : list) {
            if (bd == null) {
                throw new ValueValidationException("list may not contain null");
            }
        }
        if (op == Operation.div) {
            for (BigDecimal bd : list) {
                if (bd.equals(BigDecimal.ZERO)) {
                    throw new ValueValidationException("Elements can not be zero for devision");
                }
            }
        }

    };


    /**
     * this validator is used for integers
     */
    static final Consumer<List<BigDecimal>> integerListValidator = list -> {
        if (list.stream().anyMatch(bd -> bd.remainder(BigDecimal.ONE) != BigDecimal.ZERO)) {
            throw new ValueValidationException("All numbers must be valid integers.");
        }
    };

    /**
     * this validator is used for decimals
     */
    static final Consumer<List<BigDecimal>> decimalListValidator = list -> {
        if (list.stream().anyMatch(bd -> bd.floatValue() > bd.doubleValue() + 0.0000001)) {
            throw new ValueValidationException("All numbers must be valid floats.");
        }
    };




}
