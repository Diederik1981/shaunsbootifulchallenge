package nl.ddeboer.bootcalc.service.validation.exception;

import nl.ddeboer.bootcalc.core.CoreException;

public class ValueValidationException extends CoreException {

    public ValueValidationException(String message) {
        super(message);
    }
}
