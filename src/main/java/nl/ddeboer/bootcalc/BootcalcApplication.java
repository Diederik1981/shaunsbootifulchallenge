package nl.ddeboer.bootcalc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * run this !
 */
@SpringBootApplication
public class BootcalcApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootcalcApplication.class, args);
    }

}
